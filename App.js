import React from 'react';
import { StyleSheet, Text, View, AsyncStorage } from 'react-native';

import NewQuote from './js/components/NewQuote';
import Quote from './js/components/Quote';
import StyledButton from './js/components/StyledButton';

// Obsolete
const data = [
  {text: 'Furcht besiegt mehr Menschen als irgendetwas anderes auf der Welt.', author: 'Ralph Waldo Emerson'},
  {text: 'Ein Langweiler ist ein Mensch, der redet, wenn du wünschst, dass er zuhört.', author: 'Ambrose Bierce'},
  {text: 'Es gibt auch Spiegel, in denen man erkennen kann, was einem fehlt.', author: 'Friedrich Hebbel'},
  {text: 'Politik ist nur der Spielraum, den die Wirtschaft ihr lässt.', author: 'Dieter Hildebrandt'},
];

export default class App extends React.Component {
  state = {
    index: 0,
    showNewQuoteScreen: false,
    quotes: data
  }

  _retrieveData = async () => {
    let value  = await AsyncStorage.getItem('QUOTES');
    if(value !== null){
      console.log(JSON.parse(value));
      this.setState({quotes: JSON.parse(value)});
    }
  }

  componentDidMount = () => {
    this._retrieveData();
  }

  /**
   * Deletes the last quote on stack
   */

  _deleteQuote = () => {
    let {index, quotes} = this.state;
    quotes.splice(index, 1);

    this._storeData(quotes);
  }

  /**
   * Stores data locally
   */

  _storeData(quotes){
    this.setState({index: quotes.length-1})
    AsyncStorage.setItem('QUOTES', JSON.stringify(quotes));
  }

  _showNextQuote(next){
    const {quotes, index} = this.state;
    if(next===true){
      this.setState({index: index+1 < quotes.length ? index+1 : 0});
    }else{
      this.setState({index: index-1 < 0 ? quotes.length-1 : index-1});
    }
  }

  /**
   * Adds a new quote
   */

  _addQuote = (text, author) => {
    const { quotes } = this.state;
    quotes.push({text, author});
    if(text && author){    
      this._storeData(quotes);
      this.setState({
        quotes: quotes, showNewQuoteScreen: false
      });
    }else{
      alert('Bitte Zitat und Namen angeben!');
    }
  }

  render() {
    const {quotes, index} = this.state;
    const quote = quotes[index];
    let renderComponent = (
      <Text>Keine Zitate vorhanden!</Text>
    );

    if(quote){
      renderComponent = <Quote text={quote.text} author={quote.author}/>;
    } 

    return (
      <View style={styles.container}>
        <NewQuote visible={this.state.showNewQuoteScreen} onSave={this._addQuote}/>
        {renderComponent}
        <StyledButton 
              style={[styles.buttons, styles.buttonright]}
              title="Nächstes Zitat"
              onPress={() => this._showNextQuote(true)} />
        <StyledButton style={[styles.buttons, styles.buttonleft]} title="Voriges Zitat"
                      onPress={() => this._showNextQuote(false)}/>
        <StyledButton style={[styles.buttons, styles.buttonNew]} title="Neu"
                      onPress={() => this.setState({showNewQuoteScreen: true})}/>
        <StyledButton style={[styles.buttons, styles.buttonDelete]} title="Löschen"
                      onPress={() => this._deleteQuote()}/>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  buttons: {
    position: "absolute", 
    bottom: 20
  },  
  buttonNew:{
    top: 40,
    right: 10
  },
  buttonDelete:{
    top: 40,
    left: 10
  },
  buttonleft:{
    left: 10
  },
  buttonright:{
    right: 10
  },
  container: {
    flex: 1,
    backgroundColor: '#DDDDDD',
    alignItems: 'center',
    justifyContent: 'center',
  },
  welcome: {
    fontWeight: "bold",
    color: "red"
  }

});

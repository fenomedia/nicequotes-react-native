import React, { Component } from 'react'
import { Button, TextInput, Modal, View , StyleSheet} from 'react-native';

export default class NewQuote extends Component{

    state = {
        content: null,
        author: null
    }

    render(){
        const {visible, onSave} = this.props;
        const {content, author} = this.state;
        return (

            <Modal visible={visible} onRequestClose={() => onSave(content, author)} animationType="slide">
                <View style={styles.container}>
                    <TextInput 
                        style={[styles.textInput, {height: 150}]} 
                        placeholder="Inhalt des Zitats" 
                        onChangeText={text => this.setState({content: text})}
                        underlineColorAndroid="transparent" 
                        multiline={true}/>
                    <TextInput 
                        style={styles.textInput} 
                        placeholder="Autor/in des Zitats"
                        onChangeText={text => this.setState({author: text})}
                        underlineColorAndroid="transparent" />
                    <Button 
                        title="Speichern" 
                        onPress={() => {
                            this.setState({content: null, author: null});
                            onSave(content, author)
                        }}/>
                </View>
            </Modal>
        );
        
    }
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#DDDDDD',
        alignItems: 'center',
        justifyContent: 'flex-start',
        paddingTop: 60
    },
    textInput: {
        borderWidth: 1,
        borderColor: "deepskyblue",
        borderRadius: 4,
        width: '80%',
        marginBottom: 20,
        fontSize: 20,
        padding: 10,
        height: 150
    }
});
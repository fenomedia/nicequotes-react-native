import React, { Component } from 'react';
import { Text, StyleSheet, View } from 'react-native';


const Quote = (props) => {
    const { text, author } = props;
    return (
        <View style={commonStyle.container}>
            <Text style={commonStyle.quote}>{ text }</Text>
            <Text style={commonStyle.author}>&mdash; { author }</Text>
        </View>
    );
}


const commonStyle = StyleSheet.create({
    container:{
        padding: 20,
        backgroundColor: "white",
        borderRadius: 10,
        shadowColor: '#AAAAAA',
        elevation: 3,
        shadowOffset: {
            width: 3,
            height: 3
        },
        shadowOpacity: .5
    },
    quote: {
        fontSize: 36,
        fontStyle: 'italic',
        marginBottom: 20,
        textAlign: 'center'
    },
    author: {
        fontSize: 20, 
        marginBottom: 50,
        textAlign: "right"
    }
});

export default Quote;
import React, { Component } from 'react';
import { View, Button } from 'react-native';

class StyledButton extends Component{

    render(){
        const {onPress, style, title} = this.props;
        return (
            <View style={style}>
                <Button onPress={onPress} title={title} />
            </View>
        );
    }

};

export default StyledButton;